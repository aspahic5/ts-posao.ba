package ba.posao.repositories;

import org.springframework.data.repository.CrudRepository;
import ba.posao.models.OglasPodaci;
import org.springframework.stereotype.Repository;
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface OglasPodaciRepository extends CrudRepository<OglasPodaci, Long> {

} 
