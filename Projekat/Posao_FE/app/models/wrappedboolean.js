import BaseModel from './base-model';

var _modelProperties = ['bool'];

export default BaseModel.extend({
	modelProperties: _modelProperties,
});