package ba.posao.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ba.posao.models.Notifikacija;
import ba.posao.repositories.NotifikacijaRepository;

@Service
public class NotifikacijaService {

	private final static int PAGESIZE = 3;

    @Autowired
    NotifikacijaRepository repository;

    public Iterable<Notifikacija> findAllNotifikacija() {
        return repository.findAll();
    }
    
    public Notifikacija findNotifikacija (int id) {
        return repository.findById(id).orElse(null);
    }

    public List<Notifikacija> getPage(int pageNumber) {
        PageRequest request = PageRequest.of(pageNumber - 1, PAGESIZE, Sort.Direction.ASC, "idNotifikacije");

        return repository.findAll(request).getContent();
    }
    
    public void addNotifikacija(Notifikacija n) {
    	repository.save(n);
	}
    
    public void updateNotifikacija(Notifikacija n) {
    	repository.save(n);
	}

    public void removeNotifikacija(int id) {
        Optional<Notifikacija> notifikacija = repository.findById(id);
        if(!notifikacija.isPresent()){
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ne postoji trazena notifikacija");
        }
    	repository.delete(notifikacija.get());
	}
    
    public Notifikacija findByIdNotifikacije(Integer id) {
    	return repository.findById(id).orElse(null);
	}

}
