package ba.posao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ba.posao.models.Korisnik;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Korisnik, Long> {

}
