package ba.posao.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ba.posao.models.Oglas;
import ba.posao.models.OglasPodaci;
import ba.posao.repositories.OglasPodaciRepository;
import ba.posao.repositories.OglasRepository;

@Service
public class OglasiService {
	
	@Autowired
	OglasRepository repository;
	
	@Autowired
	OglasPodaciRepository podaciRepository;
	
 	public Boolean addOglas(Oglas k) {
		k.setDatumObjave(new Date());
		Oglas _oglas = repository.save(k);
		for(OglasPodaci podatak : k.getOglasPodaci()){
			podatak.setOglas(_oglas);
		}
		podaciRepository.saveAll(k.getOglasPodaci());
		return true;
	}
	    
	public Boolean updateOglas(Oglas k) {
		repository.save(k);
		return true;
	}

	public Boolean removeOglas(int id) {
		Optional<Oglas> oglas = repository.findById(id);
		if(!oglas.isPresent())
			return false;
		repository.delete(oglas.get());
		return true;
	}
	    
   public Boolean closeOglas(int id) {
		Optional<Oglas> oglasOptional = repository.findById(id);
		if(!oglasOptional.isPresent())
			return false;

		Oglas oglas = oglasOptional.get();
		if (oglas.getZatvoren() != 0)
			return false;

		Date date = LocalDate.now().toDate();
		oglas.setDatumIsteka(date);
		oglas.setZatvoren((byte) 1);
		repository.save(oglas);
		return true;

   }
	   
	   public Boolean reOpenOglas(int id, Integer d) {
	 		Optional<Oglas> oglasOptional = repository.findById(id);

		   if (!oglasOptional.isPresent())
		   		return false;

			Oglas oglas = oglasOptional.get();
			if(oglas.getZatvoren() != 1)
				return false;

			oglas.setDatumIsteka((DateTime.now().plusDays(d)).toDate());
			oglas.setZatvoren((byte) 0);
			repository.save(oglas);
			return true;
		}
	   
	   public List<Oglas> search(String name, int idlokacije, int idkategorije, Boolean order) {
		   if (order)
		   return repository.searchASC(name, idlokacije, idkategorije);
		   else return repository.searchDESC(name, idlokacije, idkategorije);
		   
	   }
	   
	   public List<Oglas> searchName(String name, Boolean order) {
		   if (order)
		   return repository.findAllByOglasPodaciVrijednostASC(name);
		   else return repository.findAllByOglasPodaciVrijednost(name);
	   }
	   
	   public List<Oglas> searchLocation(int idlokacije, Boolean order) {
		   if (order)
		   return repository.searchLocationASC(idlokacije);
		   else return repository.searchLocation(idlokacije);
	   }
	   public List<Oglas> searchCategory(int idkategorije, Boolean order) {
		   if (order)
		   return repository.searchKategoryASC(idkategorije);
		   else return repository.searchKategory(idkategorije);
	   }
	   public List<Oglas> searchNameCategory(String name, int idkategorije, Boolean order) {
		   if (order)
		   return repository.searchNameCategoryASC(name, idkategorije);
		   else return repository.searchNameCategory(name, idkategorije);
	   }
	   
	   public List<Oglas> searchNameLocation(String name, int idlokacije, Boolean order) {
		   if (order)
		   return repository.searchNameLocatinASC(name, idlokacije);
		   else return repository.searchNameLocatin(name, idlokacije);
	   }
	   public List<Oglas> searchCategoryLocation(int idkategorije, int idlokacije, Boolean order) {
		   if (order)
		   return repository.searchCategoryLocation(idlokacije, idkategorije);
		   else return repository.searchCategoryLocation(idlokacije, idkategorije);
	   }
	   
	   public List<Oglas> searchAll(Boolean order)
	   {
		   if (order)
			   return repository.searchAllASC();
		   else return repository.searchAll();
			   
	   }
	   
	   public int getCount() {
		   return repository.getCount();
	   }
	   
	   
	   
}

