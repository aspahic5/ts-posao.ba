package ba.posao.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ba.posao.models.Nezaposleni;
import org.springframework.stereotype.Repository;

import java.util.Optional;
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface NezaposleniRepository extends CrudRepository<Nezaposleni, Integer> {
//	@Query("select k from Nezaposleni k where idKorisnika=?")
	public Optional<Nezaposleni> findById(Integer id);
	
	@Query("SELECT COUNT(k) FROM Nezaposleni k")
	public int getUnemployedCount();
} 
