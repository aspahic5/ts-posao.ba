package ba.posao.services;

import java.util.Date;
import java.util.Optional;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import ba.posao.models.Nezaposleni;
import ba.posao.models.Oglas;
import ba.posao.models.OglasPrijave;
import ba.posao.models.Poruke;
import ba.posao.repositories.KorisnikRepository;
import ba.posao.repositories.NezaposleniRepository;
import ba.posao.repositories.OglasPrijaveRepository;
import ba.posao.repositories.OglasRepository;
import ba.posao.repositories.PorukeRepository;

@Service
public class OglasiPrijaveService {

	@Autowired
	OglasPrijaveRepository repository;
	
	@Autowired
	OglasRepository oglasRepository;
	
	@Autowired
	NezaposleniRepository nezaposleniRepository;
	
	@Autowired 
	KorisnikRepository korisnikRepository;
	
	@Autowired 
	PorukeRepository porukeRepository;
	
     public Boolean addPrijavu(int korisnik, int oglas) {

		 Optional<Nezaposleni> nezaposleniOptional = nezaposleniRepository.findById(korisnik);
		 Optional<Oglas> oglasOptional = oglasRepository.findById(oglas);

		 if (!(oglasOptional.isPresent() && nezaposleniOptional.isPresent()))
		 	return false;

		 if (repository.findByPrijava(oglas, korisnik).size() != 0)
		 	return false;

		 Poruke poruka = new Poruke();
		 poruka.setPosiljalac(korisnikRepository.findByIdKorisnika(korisnik));
		 poruka.setPrimalac(korisnikRepository.findByIdKorisnika(oglasOptional.get().getPoslodavac().getIdKorisnika()));
		 poruka.setVrijeme(LocalDate.now().toDate());

		 poruka.setTekst("Korisnik "+ nezaposleniOptional.get().getIme()+" "+nezaposleniOptional.get().getPrezime()+"se prijavio na oglas "+oglasOptional.get().getNaziv());
		 poruka.setProcitano(false);
		 porukeRepository.save(poruka);
		 OglasPrijave prijava = new OglasPrijave();
		 prijava.setOglas(oglasOptional.get());
		 prijava.setNezaposleni(nezaposleniOptional.get());
		 prijava.setVrijemePrijave(LocalDate.now().toDate());
		 repository.save(prijava);
		 return true;

	}
     
     public int getCount()  {
    	 return repository.getCount();
     }
     
     public ResponseEntity imaPrijava(int korisnik, int oglas)  {
    	 
    	 if (!nezaposleniRepository.findById(korisnik).isPresent())
    		 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ne postoji korisnik sa traženim id");
    	 else if (!oglasRepository.findById(oglas).isPresent())
    		 return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Ne postoji oglas sa traženim id");
    	 else return ResponseEntity.status(HttpStatus.OK).body(repository.findByPrijava(oglas, korisnik).size()!=0);
     }

     
}
