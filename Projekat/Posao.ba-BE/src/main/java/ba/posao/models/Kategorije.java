package ba.posao.models;

import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Pattern;

@Entity
public class Kategorije implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idkategorije")
	private Integer idkategorije;

	@Pattern(regexp = "^[A-Za-z0-9 _-ČĆŠĐŽčćšđž]*[A-Za-z0-9 _ČĆŠĐŽčćšđž-][A-Za-z0-9 _ČĆŠĐŽčćšđž]*$")
    private String naziv;

	public Integer getId() {
		return idkategorije;
	}

	public void setId(Integer idkategorije) {
		this.idkategorije = idkategorije;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
}
