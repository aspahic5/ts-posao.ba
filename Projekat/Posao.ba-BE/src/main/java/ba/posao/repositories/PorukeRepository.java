package ba.posao.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ba.posao.models.Poruke;
import org.springframework.stereotype.Repository;

@Repository
public interface PorukeRepository extends JpaRepository<Poruke, Long> {
	@Query("select p from Poruke p where idPoruke=:id")
	public Poruke findById(Integer id);	
	
	@Query("select p from Poruke p where idPrimaoca=:id")
	public List<Poruke> findByRecipient(Integer id);	
	
	@Query("select p from Poruke p where idPosiljaoca=:id")
	public List<Poruke> findBySender(Integer id);
	
	@Query("select count(p) from Poruke p where procitano=false and idPrimaoca=:id")
	public int countUnread(Integer id);
	
	
}